package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ModeloEpisodio;


public class ControleEpisodio {
	public boolean inserir(ModeloEpisodio user) {		
		boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();		
				String sql = "INSERT INTO episodio(nome,comentario) VALUES(?,?);";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, user.getNome());
				ps.setString(2, user.getComentario());
				
				if(!ps.execute()) {
					resultado = true;
				}			
			new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		return resultado;	
	}
		


	public boolean atualizar(ModeloEpisodio user) {
		boolean resultado = false;
			try {	
				Connection con = new Conexao().abrirConexao();
				String sql = "UPDATE episodio SET email=?,senha=? WHERE id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, user.getNome());
				ps.setString(2, user.getComentario());
				if(!ps.execute()) {
					resultado = true;
				}
			new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		return resultado;
	}


	public boolean deletar(ModeloEpisodio user) {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE episodio WHERE nome=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user.getNome());
			if(!ps.execute()) {
				resultado = true;			
			}			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());		
		}
		return resultado;
	}
		

public ArrayList<ModeloEpisodio> consultaEpisodio(){
	ArrayList<ModeloEpisodio> lista = new ArrayList<ModeloEpisodio>();
	try {
		Connection conn = new Conexao().abrirConexao();
		String sql = "SELECT * FROM episodio;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		if(rs != null) {
			while(rs.next()) {
				ModeloEpisodio a = new ModeloEpisodio();
			
				a.setNome(rs.getString("nome"));
				a.setComentario(rs.getString("comentario"));
				
				lista.add(a);
			}
		}
		new Conexao().fecharConexao(conn);
	}catch(SQLException e) {
		System.out.println(e.getMessage());
	}
	return lista;
}
}





