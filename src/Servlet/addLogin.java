package Servlets;
	import java.io.IOException;
	import modelo.ModeloLogin;
	import controle.ControleLogin;
	import javax.servlet.ServletException;
	import javax.servlet.http.HttpServletRequest;
	import javax.servlet.http.HttpServletResponse;

	public class addLogin {	 
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
			request.getRequestDispatcher("index.xtml").forward(request,response);
		}
		
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
			String email = request.getParameter("email");
			String senha = request.getParameter("senha");
			
			
			ModeloLogin login = new ModeloLogin();
			login.setEmail(email);
			login.setSenha(senha);
			
			boolean add = new ControleLogin().inserir(login);
			if(add) {
				response.getWriter().print("<script>alert('deu certo');window.location.href='index.xtml'</script>");
			}else {
				response.getWriter().print("<script>alert('deu errado');window.location.href='index.xhtml'</script>");
			}
		}
	}
