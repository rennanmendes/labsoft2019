package Servlets;

import java.io.IOException;
import modelo.ModeloEpisodio;
import controle.ControleEpisodio;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class addEpisodio {	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.getRequestDispatcher("index.xtml").forward(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		String nome = request.getParameter("nome");
		String comentario = request.getParameter("comentario");
		
		
		ModeloEpisodio episodio = new ModeloEpisodio();
		episodio.setNome(nome);
		episodio.setComentario(comentario);
		
		boolean add = new ControleEpisodio().inserir(episodio);
		if(add) {
			response.getWriter().print("<script>alert('deu certo');window.location.href='mensagem.xtml'</script>");
		}else {
			response.getWriter().print("<script>alert('deu errado');window.location.href='mensagem.xhtml'</script>");
		}
	}
}


