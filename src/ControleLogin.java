package controle;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import modelo.ModeloLogin;
public class ControleLogin {
	public boolean inserir(ModeloLogin user) {		
		boolean resultado = false;
			try {
				Connection con = new Conexao().abrirConexao();		
				String sql = "INSERT INTO login(email,senha,crpto) VALUES(?,?,?);";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, user.getEmail());
				ps.setString(2, user.getSenha());
				
				if(!ps.execute()) {
					resultado = true;
				}			
			new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		return resultado;	
	}
		


	public boolean atualizar(ModeloLogin user) {
		boolean resultado = false;
			try {	
				Connection con = new Conexao().abrirConexao();
				String sql = "UPDATE login SET email=?,senha=? WHERE id=?";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, user.getEmail());
				ps.setString(2, user.getSenha());
				ps.setInt(3,user.getId());
				if(!ps.execute()) {
					resultado = true;
				}
			new Conexao().fecharConexao(con);
			}catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		return resultado;
	}


	public boolean deletar(int id) throws ClassNotFoundException {
		boolean resultado = false;
		try {
			Connection con = new Conexao().abrirConexao();
			String sql = "DELETE login WHERE id=?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1,id);
			if(!ps.execute()) {
				resultado = true;			
			}			
			new Conexao().fecharConexao(con);
		}catch(SQLException e) {
			System.out.println(e.getMessage());		
		}
		return resultado;
	}
		

public ArrayList<ModeloLogin> consultaLogin(){
	ArrayList<ModeloLogin> lista = new ArrayList<ModeloLogin>();
	try {
		Connection conn = new Conexao().abrirConexao();
		String sql = "SELECT * FROM login;";
		PreparedStatement ps = conn.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		
		if(rs != null) {
			while(rs.next()) {
				ModeloLogin a = new ModeloLogin();
				a.setId(rs.getInt("id"));
				a.setEmail(rs.getString("email"));
				a.setSenha(rs.getString("senha"));
				
				lista.add(a);
			}
		}
		new Conexao().fecharConexao(conn);
	}catch(SQLException e) {
		System.out.println(e.getMessage());
	}
	return lista;
}
}


