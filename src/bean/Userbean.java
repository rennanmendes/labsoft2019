package bean;
import javax.faces.bean.ManagedBean;
import controle.ControleEpisodio;
import modelo.ModeloEpisodio;
import java.util.ArrayList;

@ManagedBean(name="UserBean")

public class Userbean {
	
	public ArrayList<ModeloEpisodio> lista = new ControleEpisodio().consultaEpisodio();
	private String nome;
	private String comentario;
	private int id;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String adicionar(){
		ModeloEpisodio mod = new ModeloEpisodio();
		ControleEpisodio com = new ControleEpisodio();
		try {
			mod.setNome(this.nome);
			mod.setComentario(this.comentario);
			com.inserir(mod);
		}catch(Exception e) {
			e.getMessage();
			
		}
		return "episodio.xtml";
		
		
	}
	public ArrayList<ModeloEpisodio> getLista(){
		return lista;
	}
	public void setLista(ArrayList<ModeloEpisodio> lista)
	{
		this.lista = lista;
	}
	public ModeloEpisodio retornarItem(int id) {
		return lista.get(id);
		
	}
	public String atualizar(){
		ModeloEpisodio mod = new ModeloEpisodio();
		ControleEpisodio com = new ControleEpisodio();
		mod.setNome(this.nome);
		mod.setComentario(this.comentario);
		if(com.atualizar(mod)) {
			return "episodio.xhtml";
		}else {
			return "index.xhtml";
		}
		
	}
	public String deletar(int id) {
		new ControleEpisodio().deletar(id);
		return "episodio.xtml";
	}
	public void selecionarId(int id) {
		ModeloEpisodio com = new ControleEpisodio().consultaEpisodio(id);
		this.setId(com.getId());
		this.setNome(com.getNome());
		this.setComentario(com.getComentario());
		
	}
}
	